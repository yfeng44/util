module gitlab.com/akita/util/v2

require (
	github.com/go-redis/redis/v8 v8.11.4
	github.com/go-sql-driver/mysql v1.5.0
	github.com/golang/mock v1.4.4
	github.com/golang/snappy v0.0.2 // indirect
	github.com/onsi/ginkgo v1.16.4
	github.com/onsi/gomega v1.16.0
	github.com/rs/xid v1.2.1
	github.com/stretchr/testify v1.6.1 // indirect
	github.com/tebeka/atexit v0.3.0
	github.com/tidwall/pretty v1.0.2 // indirect
	github.com/xdg/stringprep v1.0.0 // indirect
	gitlab.com/akita/akita/v2 v2.0.0-alpha.2
	go.mongodb.org/mongo-driver v1.4.1
	golang.org/x/crypto v0.0.0-20200820211705-5c72a883971a // indirect
	gopkg.in/yaml.v3 v3.0.0-20200615113413-eeeca48fe776 // indirect
)

// replace gitlab.com/akita/akita => ../akita

go 1.16
