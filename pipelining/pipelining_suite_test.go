package pipelining

import (
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

//go:generate mockgen -destination "mock_buffering_test.go" -package $GOPACKAGE -write_package_comment=false gitlab.com/akita/util/v2/buffering Buffer

func TestPipelining(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Pipelining Suite")
}
