package tracing

import (
	"fmt"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/akita/akita/v2/sim"
)

var _ = Describe("BusyTimeTracer", func() {
	var (
		t *BusyTimeTracer
	)

	BeforeEach(func() {
		t = NewBusyTimeTracer(nil)
	})

	It("should track busy time, one task", func() {
		t.StartTask(Task{ID: "1", StartTime: 1})
		t.EndTask(Task{ID: "1", EndTime: 2})

		Expect(t.BusyTime()).To(Equal(sim.VTimeInSec(1.0)))
	})

	It("should track busy time, two tasks", func() {
		t.StartTask(Task{ID: "1", StartTime: 1})
		t.EndTask(Task{ID: "1", EndTime: 2})

		t.StartTask(Task{ID: "2", StartTime: 3})
		t.EndTask(Task{ID: "2", EndTime: 4})

		Expect(t.BusyTime()).To(Equal(sim.VTimeInSec(2.0)))
	})

	It("should track busy time, two tasks adjacent", func() {
		t.StartTask(Task{ID: "1", StartTime: 1})
		t.EndTask(Task{ID: "1", EndTime: 2})

		t.StartTask(Task{ID: "2", StartTime: 2})
		t.EndTask(Task{ID: "2", EndTime: 3})

		Expect(t.BusyTime()).To(Equal(sim.VTimeInSec(2.0)))
	})

	It("should track busy time, two tasks overlap", func() {
		t.StartTask(Task{ID: "1", StartTime: 1})
		t.StartTask(Task{ID: "2", StartTime: 1.5})
		t.EndTask(Task{ID: "1", EndTime: 2})
		t.EndTask(Task{ID: "2", EndTime: 2.5})

		Expect(t.BusyTime()).To(Equal(sim.VTimeInSec(1.5)))
	})

	It("should track busy time, four tasks", func() {
		t.StartTask(Task{ID: "1", StartTime: 1})
		t.StartTask(Task{ID: "2", StartTime: 1.1})
		t.EndTask(Task{ID: "2", EndTime: 1.2})
		t.StartTask(Task{ID: "3", StartTime: 1.9})
		t.EndTask(Task{ID: "1", EndTime: 2})
		t.EndTask(Task{ID: "3", EndTime: 2.1})
		t.StartTask(Task{ID: "4", StartTime: 3.1})
		t.EndTask(Task{ID: "4", EndTime: 3.2})

		Expect(t.BusyTime()).To(BeNumerically("~", 1.2))
	})

	It("should be able to terminate all the tasks", func() {
		t.StartTask(Task{ID: "1", StartTime: 1})
		t.StartTask(Task{ID: "2", StartTime: 1.1})
		t.StartTask(Task{ID: "3", StartTime: 1.9})
		t.EndTask(Task{ID: "3", EndTime: 2.1})

		t.TerminateAllTasks(3.5)

		Expect(t.BusyTime()).To(BeNumerically("~", 2.5, 0.01))
	})

	Measure("It should have a good performance", func(b Benchmarker) {
		runtime := b.Time("runtime", func() {
			for i := 0; i < 10000; i++ {
				taskID := fmt.Sprintf("%d", i)
				t.StartTask(Task{
					ID:        taskID,
					StartTime: sim.VTimeInSec(i * 2),
				})
				t.EndTask((Task{
					ID:      taskID,
					EndTime: sim.VTimeInSec(i*2 + 1),
				}))
			}

			Expect(t.BusyTime()).To(BeNumerically("~", 10000, 0.01))
		})

		Expect(runtime.Seconds()).To(BeNumerically("<", 1))
	}, 10)
})
