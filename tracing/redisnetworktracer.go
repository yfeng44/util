package tracing

import (
	"context"
	"fmt"
	"os"
	"regexp"
	"strconv"
	"sync"

	"github.com/go-redis/redis/v8"
	"github.com/tebeka/atexit"
	"gitlab.com/akita/akita/v2/sim"
)

const (
	channelBufferSize int            = 2048
	timeSliceSize     sim.VTimeInSec = 0.000001000
)

type TaskWorkers struct {
	//
	// +--------------+             +----------------------+
	// |              |   channel   |                      |
	// |   End Task   +------------>|   Hungry Consumers   |
	// |  (Producer)  |  taskQueue  | (Consumers/Producers)|
	// |              |             |                      |
	// +--------------+             +----------+-----------+
	//                                 channel | writeQueue
	//                                         v
	// +--------------+             +----------------------+
	// |              |  Redis API  |                      |
	// |   Redis DB   |<------------+    Write Workers     |
	// |  (in memory) |             |     (Consumers)      |
	// |              |             |                      |
	// +--------------+             +----------------------+
	//
	taskQueue         chan Task
	writeQueue        chan string
	closeTaskQueue    chan struct{}
	closeWriteQueue   chan struct{}
	waitConsumers     *sync.WaitGroup
	waitWriteWorker   *sync.WaitGroup
	resultDict        map[string]int64 // need initiated
	numHungryConsumer int
}

// RedisNetworkTracer is a task tracer that can store the tasks of mesh tracing
// into a Redis KV database.
type RedisNetworkTracer struct {
	password     string
	ipAddress    string
	port         int
	dbNameInInt  int // need convert to int for redis.
	ctx          context.Context
	rdb          *redis.Client
	tracingTasks map[string]Task
	startTime    sim.VTimeInSec
	endTime      sim.VTimeInSec
	workers      TaskWorkers
	encoder      *NetworkTracingRecordEncoder
}

// Init establishes a connection to Redis and creates a database.
func (t *RedisNetworkTracer) Init() {
	t.getCredentials()
	t.connect()

	t.workers.LaunchHungryConsumers(t.encoder)
	t.workers.LaunchWriteWorker(t.rdb, t.ctx)
}

func (t *RedisNetworkTracer) getCredentials() {
	t.password = os.Getenv("AKITA_TRACE_PASSWORD")
	t.ipAddress = os.Getenv("AKITA_TRACE_IP")
	if t.ipAddress == "" {
		t.ipAddress = "127.0.0.1"
	}

	portString := os.Getenv("AKITA_TRACE_PORT")
	if portString == "" {
		portString = "6379"
	}
	port, err := strconv.Atoi(portString)
	if err != nil {
		panic(err)
	}
	t.port = port

	dbNameString := os.Getenv("AKITA_TRACE_REDIS_DB")
	if dbNameString == "" {
		dbNameString = "0"
	}
	dbNameInInt, err := strconv.Atoi(dbNameString)
	if err != nil {
		panic(err)
	}
	t.dbNameInInt = dbNameInInt
}

func (t *RedisNetworkTracer) connect() {
	t.rdb = redis.NewClient(&redis.Options{
		Addr:     fmt.Sprintf("%s:%d", t.ipAddress, t.port),
		Password: t.password,
		DB:       t.dbNameInInt, // set 0 then use default DB
	})
	t.ctx = context.Background()
}

// StartTask marks the start of a task.
func (t *RedisNetworkTracer) StartTask(task Task) {
	if t.endTime > 0 && task.StartTime > t.endTime {
		return
	}

	if task.ID == "" {
		panic("task id is empty")
	}

	t.tracingTasks[task.ID] = task
}

// StepTask marks a milestone during the execution of a task.
func (t *RedisNetworkTracer) StepTask(task Task) {
	// Do nothing for now
}

// EndTask writes the task into the database.
func (t *RedisNetworkTracer) EndTask(task Task) {
	if t.startTime > 0 && task.EndTime < t.startTime {
		delete(t.tracingTasks, task.ID)
		return
	}

	originalTask, ok := t.tracingTasks[task.ID]
	if !ok {
		// fmt.Println("task is not started")
		return
	}

	originalTask.EndTime = task.EndTime
	originalTask.Detail = task.Detail // seems always `nil` in consumer routines
	delete(t.tracingTasks, task.ID)

	t.workers.taskQueue <- originalTask
}

func DiscretizeVTimeInSecToSlicedTimeInInt(time sim.VTimeInSec) (int64, bool) {
	var idx int64 = -1
	// like floor() method
	for time >= 0.0 {
		time -= timeSliceSize
		idx++
	}
	return idx, time < 0.0 // true: has precision loss
}

func (w *TaskWorkers) WriteDictToRedis(
	rdb *redis.Client,
	ctx context.Context,
) {
	for k, v := range w.resultDict {
		err := rdb.Set(ctx, k, v, 0).Err()
		if err != nil {
			panic(err)
		}
	}
}

func (w *TaskWorkers) ValueAdd(key string) {
	w.writeQueue <- key // prevent goroutine racing for map[key]
}

func (w *TaskWorkers) LaunchWriteWorker(
	rdb *redis.Client,
	ctx context.Context,
) {
	w.waitWriteWorker.Add(1)
	go func() {
		finished := false
		for !finished {
			select {
			case key := <-w.writeQueue:
				// w.resultDict[key]++
				var value int64
				valueInString, err := rdb.Get(ctx, key).Result()
				if err == redis.Nil {
					valueInString = "0"
				} else if err != nil {
					panic(err)
				}
				value, err = strconv.ParseInt(valueInString, 10, 64)
				if err != nil {
					panic(err)
				}
				err = rdb.Set(ctx, key, value+1, 0).Err()
				if err != nil {
					panic(err)
				}
			case <-w.closeWriteQueue:
				finished = true
			}
		}
		// w.WriteDictToRedis(rdb, ctx)
		w.waitWriteWorker.Done()
	}()
}

func (w *TaskWorkers) LaunchHungryConsumers(encoder *networkRecordEncoder) {
	for i := 0; i < w.numHungryConsumer; i++ {
		w.waitConsumers.Add(1)
		go func() {
			finished := false
			for !finished {
				select {
				case task := <-w.taskQueue:
					if task.What != "flit_through_channel" {
						break // jump out of select, continue loop
					}
					// calculate the interval coverage of sliced time
					startSlice, _ := DiscretizeVTimeInSecToSlicedTimeInInt(task.StartTime)
					endSlice, loss := DiscretizeVTimeInSecToSlicedTimeInInt(task.EndTime)
					if loss {
						endSlice += 1
					}

					// example: "GPU1.SW_3_1_0.Port4->GPU1.SW_4_1_0.Port2" (Port[0-4])
					// index:       0    1 2 3     4     5    6 7 8     9
					re := regexp.MustCompile(`\d{1,}`)
					extractInString := re.FindAllString(task.Where, -1)
					ext := make([]int, 0, len(extractInString))
					for _, v := range extractInString {
						temp, err := strconv.Atoi(v)
						if err != nil {
							panic(err)
						}
						ext = append(ext, temp)
					}

					if ext[0] != ext[5] {
						panic("Redis network tracer doesnot support switch connection " +
							"across different GPUs")
					}
					gpuID := ext[0]
					tileFrom := [3]int{ext[1], ext[2], ext[3]}
					tileTo := [3]int{ext[6], ext[7], ext[8]}
					// example: "flit.*mem.ReadReq"
					// index:    0    5
					msgType := task.Kind[5:]

					for sliceID := startSlice; sliceID <= endSlice; sliceID++ {
						w.ValueAdd(
							encoder.Marshal(sliceID, gpuID, tileFrom, tileTo, msgType),
						)
					}
				case <-w.closeTaskQueue:
					finished = true
				}
			}
			w.waitConsumers.Done()
		}()
	}
}

// NewRedisNetworkTracerWithTimeRange creates a RedisTracer which can only trace
// the tasks that at least partially overlaps with the given start and end time.
// If the start time is negative, the tracer will start tracing at the beginning
// of the simulation. If the end time is negative, the tracer will not stop
// tracing until the end of the simulation.
func NewRedisNetworkTracerWithTimeRange(
	startTime, endTime sim.VTimeInSec,
	numHungryConsumer int,
	encoder *NetworkTracingRecordEncoder,
) *RedisNetworkTracer {
	if startTime >= 0 && endTime >= 0 {
		if startTime >= endTime {
			panic("startTime cannot be greater than endTime")
		}
	}

	t := &RedisNetworkTracer{
		startTime:    startTime,
		endTime:      endTime,
		tracingTasks: make(map[string]Task),
		workers: TaskWorkers{
			taskQueue:         make(chan Task, channelBufferSize),
			writeQueue:        make(chan string, channelBufferSize),
			closeTaskQueue:    make(chan struct{}), // taskQueue close signal
			closeWriteQueue:   make(chan struct{}), // writeQueue close signal
			waitConsumers:     new(sync.WaitGroup),
			waitWriteWorker:   new(sync.WaitGroup),
			resultDict:        make(map[string]int64),
			numHungryConsumer: numHungryConsumer,
		},
		encoder: encoder,
	}

	atexit.Register(func() {
		close(t.workers.closeTaskQueue)
		t.workers.waitConsumers.Wait()
		close(t.workers.closeWriteQueue)
		t.workers.waitWriteWorker.Wait()
	})

	return t
}

// NewRedisNetworkTracer returns a new RedisNetworkTracer.
func NewRedisNetworkTracer(
	numHungryConsumer int,
	encoder *NetworkTracingRecordEncoder,
) *RedisNetworkTracer {
	t := &RedisNetworkTracer{
		startTime:    -1,
		endTime:      -1,
		tracingTasks: make(map[string]Task),
		workers: TaskWorkers{
			taskQueue:         make(chan Task, channelBufferSize),
			writeQueue:        make(chan string, channelBufferSize),
			closeTaskQueue:    make(chan struct{}), // taskQueue close signal
			closeWriteQueue:   make(chan struct{}), // writeQueue close signal
			waitConsumers:     new(sync.WaitGroup),
			waitWriteWorker:   new(sync.WaitGroup),
			resultDict:        make(map[string]int64),
			numHungryConsumer: numHungryConsumer,
		},
		encoder: encoder,
	}

	atexit.Register(func() {
		close(t.workers.closeTaskQueue)
		t.workers.waitConsumers.Wait()
		close(t.workers.closeWriteQueue)
		t.workers.waitWriteWorker.Wait()
	})

	return t
}

/* Common message types for reference:
var DefaultSupportedMsgType = []string{
	"*cache.FlushReq",
	"*cache.FlushRsp",
	"*mem.DataReadyRsp",
	"*mem.ReadReq",
	"*mem.WriteDoneRsp",
	"*mem.WriteReq",
	"*protocol.FlushReq",
	"*protocol.LaunchKernelReq",
	"*protocol.MapWGReq",
	"*protocol.MemCopyD2HReq",
	"*protocol.MemCopyH2DReq",
	"*protocol.WGCompletionMsg",
	"*vm.TranslationReq",
	"*vm.TranslationRsp",
}
*/

type NetworkTracingRecordEncoder struct {
	typeToID   map[string]int
	onlyOneGPU bool
	only2DMesh bool
}

type networkRecordEncoder = NetworkTracingRecordEncoder

func NewNetworkTracingRecordEncoder(
	msgTypes []string,
) *NetworkTracingRecordEncoder {
	encoder := &NetworkTracingRecordEncoder{
		typeToID:   make(map[string]int),
		onlyOneGPU: false,
		only2DMesh: false,
	}
	for idx, t := range msgTypes {
		encoder.typeToID[t] = idx
	}
	return encoder
}

func (e *networkRecordEncoder) OnlyOneGPU() *networkRecordEncoder {
	e.onlyOneGPU = true
	return e
}

func (e *networkRecordEncoder) Only2DMesh() *networkRecordEncoder {
	e.only2DMesh = true
	return e
}

func (e *networkRecordEncoder) Marshal(
	timeSlice int64,
	gpuID int,
	tileFrom [3]int,
	tileTo [3]int,
	msgType string,
) string {
	var from, to string

	if e.only2DMesh {
		from = fmt.Sprintf("%d_%d", tileFrom[0], tileFrom[1])
		to = fmt.Sprintf("%d_%d", tileTo[0], tileTo[1])
	} else {
		from = fmt.Sprintf("%d_%d_%d", tileFrom[0], tileFrom[1], tileFrom[2])
		to = fmt.Sprintf("%d_%d_%d", tileTo[0], tileTo[1], tileTo[2])
	}

	if !e.onlyOneGPU {
		from = fmt.Sprintf("%d.%s", gpuID, from)
		to = fmt.Sprintf("%d.%s", gpuID, to)
	}

	typeID, ok := e.typeToID[msgType]
	if !ok {
		panic(
			"Undefined message type " + msgType + " in NetworkTracingRecordEncoder")
	}

	return fmt.Sprintf("%d@%d#%s>%s", timeSlice, typeID, from, to)
}
